//
//  MoviesViewController.m
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import "MoviesViewController.h"
#import "UIHelper.h"
#import <SDWebImage/UIImageView+WebCache.h>


@interface MoviesViewController ()

@end

@implementation MoviesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _presenter = [[MoviesPresenterImpl alloc] init];
    [_presenter initWithView:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
    


- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    
    Movie *movie = self.movies[indexPath.row];
    
    UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cellMovie" forIndexPath:indexPath];
    UIImageView *imageMovie = [cell viewWithTag:1000];
    UILabel *lbNameMovie = [cell viewWithTag:1001];
    
    [imageMovie sd_setImageWithURL:[NSURL URLWithString:movie.coverUrl]];
    lbNameMovie.text = movie.title;
    
    return cell;
    
    
}
    
- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
 
    return _movies.count;
}
    
- (void)didReceiveMovies:(NSArray *)movies {
    
    self.movies = movies;
    [self.cvMovies reloadData];
}
    
- (void)showErrorMessage:(NSString *)message {
 
    [UIHelper mostrarAlertaErro:message viewController:self];
}
    
- (void)showHideProgress:(BOOL)show {
    if(show)
        [UIHelper showProgress];
    else
    [UIHelper hideProgress];
    
}
    
    @end
