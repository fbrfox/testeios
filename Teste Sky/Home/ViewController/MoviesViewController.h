//
//  MoviesViewController.h
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoviesView.h"
#import "MoviesPresenterImpl.h"
#import "Movie.h"




@interface MoviesViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource,MoviesView>

@property MoviesPresenterImpl *presenter;
    @property (weak, nonatomic) IBOutlet UICollectionView *cvMovies;
    
    
    @property NSArray* movies;
    
    
@end
