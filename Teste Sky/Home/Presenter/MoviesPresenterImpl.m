//
//  MoviesPresenterImpl.m
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import "MoviesPresenterImpl.h"
#import "MoviesResponse.h"
#import "Movie.h"
#import "APIClient.h"
#import "MoviesView.h"
#import "UIHelper.h"

@implementation MoviesPresenterImpl
    
    
@synthesize view;
    
    
-(void)initWithView:(id<MoviesView>)theView{
    
    self.view = theView;
    
    [self getMovies];
}

    
    -(void)getMovies{
        
        [view showHideProgress:YES];
        
        [[APIClient sharedManager] getMovies:^(MoviesResponse *response, NSString *errorMessage) {
            
            [self.view showHideProgress:NO];
            
            if(errorMessage){
                [self.view showErrorMessage:errorMessage];
            }
            else{
                
                [self.view didReceiveMovies:response.movies];
            }
            
            
        }];
        
        
        
        
    }
    
@end
