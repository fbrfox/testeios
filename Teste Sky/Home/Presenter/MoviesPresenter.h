//
//  MoviesPresenter.h
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MoviesView;

@protocol MoviesPresenter <NSObject>

- (void)initWithView:(id <MoviesView>)theView;
    
@property(nonatomic, strong) id <MoviesView> view;
    
@end
