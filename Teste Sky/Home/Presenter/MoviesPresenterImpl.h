//
//  MoviesPresenterImpl.h
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MoviesPresenter.h"


@interface MoviesPresenterImpl : NSObject<MoviesPresenter>

@end
