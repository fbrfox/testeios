//
//  MoviesView.h
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol MoviesView <NSObject>

-(void) didReceiveMovies:(NSArray *)movies;
    
-(void)showHideProgress:(BOOL)show;

-(void)showErrorMessage:(NSString *)message;
    
    
@end
