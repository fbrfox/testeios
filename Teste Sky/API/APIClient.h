//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//


#import <AFNetworking.h>
#import "MoviesResponse.h"

@interface APIClient : AFHTTPSessionManager

+ (APIClient *)sharedManager;



- (NSURLSessionDataTask *)getMovies:(void (^)(MoviesResponse *response, NSString *errorMessage))completion;
    
    
@end
