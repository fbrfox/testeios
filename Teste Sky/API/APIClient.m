//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import "APIClient.h"
#import "MoviesResponse.h"

//EndPoints Urls


NSString *const APIBaseURL = @"https://sky-exercise.herokuapp.com/api/";

//NSString *const APIBaseURL = @"http://192.168.43.44/tripholics/api/";

//Producao
NSString *const MOVIES_URL = @"Movies";


@implementation APIClient

+ (APIClient *)sharedManager {
    static APIClient *_sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];

        NSLog(APIBaseURL);
        
        _sharedManager = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:APIBaseURL] sessionConfiguration:configuration];
    
    });

    return _sharedManager;
}

- (NSURLSessionDataTask *)getMovies:(void (^)(MoviesResponse *response, NSString *errorMessage))completion {

    NSURLSessionDataTask *task = [self GETWithLogParams:MOVIES_URL parameters:[NSDictionary new]
                                                success:^(NSURLSessionDataTask *task, id responseObject) {

                                                    NSError *error;
                                                    
                                                    NSDictionary *dic = @{@"movies":responseObject};
                                                                          

                                                    MoviesResponse *baseResponse = [[MoviesResponse alloc] initWithDictionary:dic error:&error];
                                                    if (error) {
                                                        completion(nil, @"Server Down");
                                                    } else {
                                                        completion(baseResponse, nil);
                                                    }

                                                } failure:^(NSURLSessionDataTask *task, NSError *error) {

                if (((NSHTTPURLResponse *) [task response]).statusCode == 401) {
                    completion(nil, @"UNauthorized");
                } else {
                    completion(nil, @"Server Down");
                }
            }];

    return task;

}



//Metodo que encapsula o método de Get somente para logar os parametros em desenvolvimento
- (NSURLSessionDataTask *)GETWithLogParams:(NSString *)URLString
                                parameters:(NSDictionary *)parameters
                                   success:(void (^)(NSURLSessionDataTask *task, id responseObject))success
                                   failure:(void (^)(NSURLSessionDataTask *task, NSError *error))failure {

    for (NSString *key in parameters.allKeys) {

        URLString = [URLString stringByAppendingString:[NSString stringWithFormat:@"/%@/%@", key, parameters[key]]];
    }

#if DEBUG
    NSError *errorAutentication;
    NSData *jsonDataAutentication = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&errorAutentication];
    if (!errorAutentication) {
        NSLog(@"GET URL :%@ >>>>>>>>>>>>> Json params:%@", URLString, [[NSString alloc] initWithData:jsonDataAutentication encoding:NSUTF8StringEncoding]);
    }
#endif
    

    NSURLSessionDataTask *task = [self GET:URLString parameters:nil progress:nil
        success:^(NSURLSessionDataTask *task, id responseObject) {
            success(task, responseObject);
        }
        failure:^(NSURLSessionDataTask *task, NSError *error) {
#if DEBUG
            NSLog(@"%@", error);
#endif
            failure(task, error);
        }];

    return task;
}



@end
