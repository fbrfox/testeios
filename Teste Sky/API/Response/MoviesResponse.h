//
//  MoviesResponse.h
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import "JSONModel.h"
@protocol Movie;

@interface MoviesResponse : JSONModel
    
    @property NSArray<Movie>* movies;

@end
