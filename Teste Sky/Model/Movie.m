//
//  Movie.m
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import "Movie.h"

@implementation Movie

    
    +(JSONKeyMapper *)keyMapper{
        
        return [[JSONKeyMapper alloc]initWithDictionary:@{@"cover_url":@"coverUrl"}];
        
    }
    
@end
