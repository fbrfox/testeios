//
//  Movie.h
//  Teste Sky
//
//  Created by Pedro  on 06/09/2018.
//  Copyright © 2018 Pedro . All rights reserved.
//

#import "JSONModel.h"


@protocol Movie <NSObject>

@end
    
    

@interface Movie : JSONModel

    @property NSString *title;
    @property NSString *coverUrl;
    
@end
