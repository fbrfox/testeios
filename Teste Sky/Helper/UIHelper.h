//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>



@protocol UIHelperDelegate <NSObject>

@optional

-(void)alertControllerOkClicked;
-(void)alertControllerSimClicked;
-(void)alertControllerNaoClicked;

@end

@interface UIHelper : NSObject





//+ (UIHelper *)sharedInstance;

+ (void)mostrarAlerta:(NSString *)mensagem  viewController:(UIViewController *)vc;

+ (void)mostrarAlertaErro:(NSString *)mensagem  viewController:(UIViewController *)vc;

+ (void)mostrarAlertaInfo:(NSString *)mensagem  viewController:(UIViewController *)vc;

+ (void)mostrarAlertaSucesso:(NSString *)mensagem viewController:(UIViewController *)vc;

+ (void)mostrarAlertaSucesso:(NSString *)mensagem viewController:(UIViewController *)vc delegate:(id<UIHelperDelegate>)delegate ;


+ (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem delegate:(id<UIHelperDelegate>)delegate viewController:(UIViewController *)vc;

+ (void)mostraAlertaConfimacao:(NSString *)texto delegate:(id <UIHelperDelegate>)delegate viewController:(UIViewController *)vc;



+ (void)showProgress;

+ (void)showProgresswithText:(NSString *)text;

+(void)hideProgress;

@end
