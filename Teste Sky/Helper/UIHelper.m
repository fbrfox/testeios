//
// Created by pedro henrique borges on 3/5/15.
// Copyright (c) 2015 Iterative. All rights reserved.
//

#import "AppDelegate.h"
#import "UIHelper.h"
#import "SVProgressHUD.h"





@implementation UIHelper

//+ (UIHelper *)sharedInstance {
//
//    static UIHelper *_sharedManager = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        _sharedManager = [[UIHelper alloc] init];
//    });
//
//    return _sharedManager;
//}

+ (void)mostrarAlerta:(NSString *)mensagem  viewController:(UIViewController *)vc{

    [self mostrarAlerta:@"" mensagem:mensagem viewController:vc];
}

+ (void)mostrarAlertaErro:(NSString *)mensagem  viewController:(UIViewController *)vc{

    [self mostrarAlerta:@"Ocorreu um erro!" mensagem:mensagem viewController:vc];
}

+ (void)mostrarAlertaInfo:(NSString *)mensagem  viewController:(UIViewController *)vc{

    [self mostrarAlerta:@"Atenção" mensagem:mensagem  viewController:vc];
}

+ (void)mostrarAlertaSucesso:(NSString *)mensagem  viewController:(UIViewController *)vc{
    
    [self mostrarAlertaSucesso:mensagem viewController:vc delegate:nil];
}


+ (void)mostrarAlertaSucesso:(NSString *)mensagem viewController:(UIViewController *)vc delegate:(id<UIHelperDelegate>)delegate {

    [self mostrarAlerta:@"Sucesso" mensagem:mensagem delegate:delegate viewController:vc] ;
}

+ (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem  viewController:(UIViewController *)vc {

    [self mostrarAlerta:titulo mensagem:mensagem delegate:nil viewController:vc];
}

+ (void)mostrarAlerta:(NSString *)titulo mensagem:(NSString *)mensagem delegate:(id<UIHelperDelegate>)delegate viewController:(UIViewController *)vc {
   
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:titulo message:mensagem preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {


        [delegate alertControllerOkClicked];
    
        
    }]];
    
    
    [vc presentViewController:alertController animated:YES completion:nil];
    
}

+(void)mostraAlertaConfimacao:(NSString *)texto delegate:(id<UIHelperDelegate>)delegate viewController:(UIViewController *)vc{

    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Confirme sua ação" message:texto
                                                                      preferredStyle:UIAlertControllerStyleAlert];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Sim" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        [delegate alertControllerSimClicked];


    }]];

    [alertController addAction:[UIAlertAction actionWithTitle:@"Não" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {

        [delegate alertControllerNaoClicked];


    }]];


    [vc presentViewController:alertController animated:YES completion:nil];
}



+ (void)showProgress {

    [self showProgresswithText:nil];

}

+ (void)showProgresswithText:(NSString *)text {

    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    if (!text) {
        
        [SVProgressHUD show];
    } else {
        [SVProgressHUD showWithStatus:text];
    }
}


+(void)hideProgress{

    [SVProgressHUD dismiss];
}






@end
